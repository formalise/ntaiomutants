<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE nta PUBLIC '-//Uppaal Team//DTD Flat System 1.6//EN' 'http://www.it.uu.se/research/group/darts/uppaal/flat-1_6.dtd'>
<nta>
	<declaration>/**
Injector workcycle configuration
flowrate: Required flow ([L^3 T^-1]; given in liters per minute )
FiO2: Fraction of inspirated oxygen, given in percentage of oxygen in the air
**/
int[0,500] flowrate = 12;
int[1,100] FiO2 = 10;
int mix=0;
/**
Duty: The opening of the injector's valve, where 100 is fully opened and 0 is fully closed.
**/
int[0,100] airDuty;
int[0,100] o2Duty;


/**
Input params:
Inspiration time (tInsp) cs
Expiration time  (tExp)  cs
Inspiration pause (pInsp)cs
Expiration pause  (pExp) cs
Flowrate 
FiO2
*/
typedef int[-100000000,10000000] big_t;

const int tInsp=100;
const int tExp=225;
const int pInsp=50;
const int pExp=10;
const int totalT = tInsp + tExp;

broadcast chan mixSupply, supplyOff, airOn, O2on;
broadcast chan reset, resetExp;
broadcast chan inspiration, expiration;
broadcast chan endSetup;
broadcast chan expOn, expOff;
broadcast chan initSetup;
big_t Q; // Int representation of q

const double PI = 3.141592653589793115997963468544185161590576171875;
const double d1 = 0.0254; // valve's diameter (m)
const double l1 = 3.0; // hose lenght 1 (m)
const double l2 = 0.3; // hose lenght 2 (m)
const double dt = 0.08; // time window (s)
const double w = 1.81E-5; // viscosity (Ns / m^2)
const double R = 8.31446261815324; // universal gas constant (J/(mol*K))
const double rho = 0.044; // air density in the lungs (kg/m^3)
double pressure1 = 68947.6; // initial pressure in tank 1 (Pa)
double pressure2 = 6894.76; // initial pressure in tank 2 (Pa)
double vol1 = 4.25E-3;  // tank volume 1 (m^3)
const double vol2 = 1.0E-3; // tank volume 2 (m^3)

double q;
big_t p1 = 68948; // initial pressure in tank 1 (Psi)
big_t p2 = 6895; // initial pressure in tank 2 (Psi)

big_t getQ(big_t p_i, big_t p_j, double l) {
    double div = l*128*w;
    if (div == 0) {
        div = 1;
    }
    return fint((10e3*PI * (p_i-p_j) * pow(d1,4)) / (div));
}

big_t getDelta() {
    double delta = ((R*dt*(Q/10e3)*rho)/vol2);
    return fint(delta);
}

/**
Variables used when making the model SMC-compatible
see: UPPAAL-SMC tutorial (https://doi.org/10.1007/s10009-014-0361-y)
**/
clock a, timeExpV;
</declaration>
	<template>
		<name x="5" y="5">Setup</name>
		<declaration>clock t;
/**
Injector workcycle configuration
FiO2: Fraction of inspirated oxygen, given in percentage of oxygen in the air
Duty: The opening of the injector's valve, where 100 is fully opened and 0 is fully closed.
**/
int[0,500] o2InAir;
int[0,500] o2WithoutAir;

/**

            
The following table matches the duty for a given flowrate. e.g. if a flowrate of 220 is required,
the injectors should open 40%.
This data was provided by profs. José I. García and Andrés M. Valencia. It was acquired experimentally and  
it is specific to injectors of the the mechanical ventilator featured in the article.

|flowrate|duty (%) |
--------------------
| 120    | 30      |
| 220    | 40      |
| 280    | 50      |
| 350    | 60      |
| 380    | 70      |
| 410    | 80      |
| 450    | 90      |
| 500    | 100     |

These points could be extrapolated with a 2nd grade polynomial
f(x) = 0,0001x^2+0,1441x+3,6656
**/

int getDuty(int flow){
    return  (flow &lt;= 120)?
    30:
    (flow &lt;= 220)?
    40:
    (flow &lt;= 280)?
    50:
    (flow &lt;= 350)?
    60:
    (flow &lt;= 380)?
    70:
    (flow &lt;= 410)?
    80:
    (flow &lt;= 450)?
    90:
    100; //more than 450, expected until 500
}

big_t setPressure1(int airflow) {
    return fint((R*rho*airflow*37)/vol2);
}


        </declaration>
		<location id="id0" x="-68" y="17">
			<name x="-78" y="-17">s0</name>
			<committed/>
		</location>
		<location id="id1" x="153" y="17">
			<name x="143" y="-17">s1</name>
			<committed/>
		</location>
		<location id="id2" x="365" y="17">
			<name x="355" y="-17">s2</name>
			<committed/>
		</location>
		<location id="id3" x="552" y="17">
			<name x="542" y="-17">s3</name>
			<committed/>
		</location>
		<location id="id4" x="552" y="144">
			<name x="578" y="136">Idle</name>
		</location>
		<init ref="id4"/>
		<transition id="id5">
			<source ref="id4"/>
			<target ref="id0"/>
			<label kind="synchronisation" x="238" y="153">initSetup?</label>
			<nail x="-68" y="144"/>
		</transition>
		<transition id="id6">
			<source ref="id3"/>
			<target ref="id4"/>
			<label kind="synchronisation" x="578" y="68">endSetup!</label>
			<label kind="assignment" x="578" y="85">p1=setPressure1(flowrate)</label>
		</transition>
		<transition id="id7">
			<source ref="id2"/>
			<target ref="id3"/>
			<label kind="assignment" x="391" y="-59">airDuty=getDuty(o2WithoutAir),
o2Duty=getDuty(o2InAir)</label>
		</transition>
		<transition id="id8">
			<source ref="id1"/>
			<target ref="id2"/>
			<label kind="assignment" x="144" y="42">o2InAir=fint((flowrate*FiO2)/100),
o2WithoutAir=flowrate-o2InAir</label>
		</transition>
		<transition id="id9">
			<source ref="id0"/>
			<target ref="id1"/>
			<label kind="select" x="-59" y="-51">fio2Actual: int[1,10]</label>
			<label kind="assignment" x="-51" y="-25">FiO2 = fio2Actual*10</label>
		</transition>
	</template>
	<template>
		<name>Control</name>
		<declaration>clock  pause;
//clock a;
int waitd = fint(dt*100); // wait duration, the value of dt in cs.
big_t resetP2() {
    pressure2 = 6984.76;
    return 6985;
}</declaration>
		<location id="id10" x="-204" y="-688">
			<name x="-187" y="-697">Start</name>
			<label kind="invariant" x="-306" y="-697">pause&lt;=50</label>
		</location>
		<location id="id11" x="-204" y="-408">
			<name x="-214" y="-442">mid</name>
			<urgent/>
		</location>
		<location id="id12" x="-8" y="-408">
			<name x="17" y="-425">openInsp</name>
			<label kind="invariant" x="17" y="-399">pause&lt;=8</label>
		</location>
		<location id="id13" x="-8" y="-110">
			<name x="9" y="-119">Insp</name>
			<urgent/>
		</location>
		<location id="id14" x="204" y="-399">
			<name x="221" y="-416">InsPause</name>
			<label kind="invariant" x="221" y="-399">pause&lt;=50</label>
		</location>
		<location id="id15" x="-408" y="-408">
			<name x="-485" y="-425">openExp</name>
			<label kind="invariant" x="-527" y="-408">pause&lt;=8</label>
		</location>
		<location id="id16" x="-408" y="-110">
			<name x="-451" y="-118">Exp</name>
			<urgent/>
		</location>
		<location id="id17" x="-578" y="-399">
			<name x="-671" y="-416">ExpPause</name>
			<label kind="invariant" x="-680" y="-391">pause&lt;=10</label>
		</location>
		<location id="id18" x="-204" y="-790">
			<name x="-214" y="-824">Setup</name>
		</location>
		<location id="id19" x="-348" y="-790">
			<name x="-358" y="-824">Idle</name>
			<urgent/>
		</location>
		<location id="id20" x="-263" y="-620">
			<name x="-273" y="-654">q</name>
			<urgent/>
		</location>
		<location id="id21" x="-144" y="-620">
			<name x="-154" y="-654">p</name>
			<urgent/>
		</location>
		<location id="id22" x="-204" y="-535">
			<name x="-214" y="-569">qp</name>
			<urgent/>
		</location>
		<init ref="id19"/>
		<transition id="id23">
			<source ref="id17"/>
			<target ref="id11"/>
			<label kind="guard" x="-442" y="-518">pause==10</label>
			<label kind="assignment" x="-408" y="-484">a=0,
Q=0</label>
			<nail x="-399" y="-493"/>
		</transition>
		<transition id="id24">
			<source ref="id22"/>
			<target ref="id11"/>
			<label kind="synchronisation" x="-195" y="-493">mixSupply!</label>
			<label kind="assignment" x="-195" y="-476">a=0</label>
		</transition>
		<transition id="id25">
			<source ref="id20"/>
			<target ref="id22"/>
			<label kind="synchronisation" x="-297" y="-586">airOn!</label>
		</transition>
		<transition id="id26">
			<source ref="id21"/>
			<target ref="id22"/>
			<label kind="synchronisation" x="-170" y="-577">O2on!</label>
		</transition>
		<transition id="id27">
			<source ref="id10"/>
			<target ref="id21"/>
			<label kind="guard" x="-161" y="-663">pause==50</label>
			<label kind="synchronisation" x="-161" y="-680">airOn!</label>
		</transition>
		<transition id="id28">
			<source ref="id10"/>
			<target ref="id20"/>
			<label kind="guard" x="-357" y="-663">pause==50</label>
			<label kind="synchronisation" x="-280" y="-671">O2on!</label>
		</transition>
		<transition id="id29">
			<source ref="id19"/>
			<target ref="id18"/>
			<label kind="synchronisation" x="-314" y="-816">initSetup!</label>
		</transition>
		<transition id="id30">
			<source ref="id15"/>
			<target ref="id16"/>
			<label kind="assignment" x="-399" y="-263">Q=getQ(0,p2,l2),
p2=p2+getDelta()</label>
		</transition>
		<transition id="id31">
			<source ref="id18"/>
			<target ref="id10"/>
			<label kind="synchronisation" x="-195" y="-756">endSetup?</label>
		</transition>
		<transition id="id32">
			<source ref="id13"/>
			<target ref="id11"/>
			<label kind="guard" x="-119" y="-297">a&lt;100</label>
		</transition>
		<transition id="id33">
			<source ref="id16"/>
			<target ref="id11"/>
			<label kind="guard" x="-357" y="-136">a&lt;325</label>
			<nail x="-204" y="-110"/>
		</transition>
		<transition id="id34">
			<source ref="id16"/>
			<target ref="id17"/>
			<label kind="guard" x="-612" y="-314">a&gt;=325</label>
			<label kind="synchronisation" x="-578" y="-272">expOff!</label>
			<label kind="assignment" x="-561" y="-238">pause=0</label>
		</transition>
		<transition id="id35">
			<source ref="id11"/>
			<target ref="id15"/>
			<label kind="guard" x="-340" y="-433">a&gt;=100</label>
			<label kind="synchronisation" x="-306" y="-408">expOn!</label>
			<label kind="assignment" x="-374" y="-408">pause=0</label>
		</transition>
		<transition id="id36">
			<source ref="id14"/>
			<target ref="id11"/>
			<label kind="guard" x="-76" y="-510">a&lt;325 &amp;&amp; pause==50</label>
			<nail x="8" y="-493"/>
		</transition>
		<transition id="id37">
			<source ref="id13"/>
			<target ref="id14"/>
			<label kind="guard" x="76" y="-212">a&gt;=100</label>
			<label kind="assignment" x="110" y="-263">Q=0,
pause=0</label>
		</transition>
		<transition id="id38">
			<source ref="id12"/>
			<target ref="id13"/>
			<label kind="assignment" x="0" y="-348">Q=getQ(p1,p2,l1),
p2=p2+getDelta()</label>
		</transition>
		<transition id="id39">
			<source ref="id11"/>
			<target ref="id12"/>
			<label kind="guard" x="-119" y="-433">a&lt;100</label>
			<label kind="assignment" x="-127" y="-408">pause=0</label>
		</transition>
	</template>
	<template>
		<name>Injector</name>
		<declaration>clock t;

/**
Using the inverse of the flowrate vs. duty table

| duty     | flow   |
---------------------
|    30    |   120  |
|    40    |   220  |
|    50    |   280  |
|    60    |   350  |
|    70    |   380  |
|    80    |   410  |
|    90    |   450  |
|    100   |   500  |

and by completing low values with the extrapolated function

| duty     | flow   |
---------------------
|    00    |   0    |
|    10    |   50   |
|    20    |   100  |

**/
int params[11] = {0,50,100,120,220,280,350,380,410,450,500};

int getFlowWithPassage(int n){
    return params[n/10];
}</declaration>
		<location id="id40" x="68" y="127">
			<name x="43" y="144">off</name>
		</location>
		<location id="id41" x="289" y="127">
			<name x="281" y="144">on</name>
			<label kind="invariant" x="280" y="161">t&lt;=100</label>
		</location>
		<init ref="id40"/>
		<transition id="id42">
			<source ref="id41"/>
			<target ref="id41"/>
			<label kind="synchronisation" x="272" y="8">mixSupply?</label>
			<label kind="assignment" x="272" y="25">mix=getFlowWithPassage(airDuty)</label>
			<nail x="272" y="68"/>
			<nail x="331" y="68"/>
		</transition>
		<transition id="id43">
			<source ref="id41"/>
			<target ref="id40"/>
			<label kind="guard" x="153" y="25">t&gt;=100</label>
			<label kind="assignment" x="153" y="8">t=0</label>
			<nail x="179" y="42"/>
		</transition>
		<transition id="id44">
			<source ref="id40"/>
			<target ref="id41"/>
			<label kind="synchronisation" x="153" y="136">airOn?</label>
			<label kind="assignment" x="161" y="161">t=0</label>
		</transition>
	</template>
	<template>
		<name>O2Valve</name>
		<declaration>clock t;
int params[11] = {0,50,100,120,220,280,350,380,410,450,500};

int getFlowWithPassage(int n){
    return params[n/10];
}</declaration>
		<location id="id45" x="-433" y="-42">
			<name x="-458" y="-25">off</name>
		</location>
		<location id="id46" x="-212" y="-42">
			<name x="-220" y="-25">on</name>
			<label kind="invariant" x="-221" y="-8">t&lt;=100</label>
		</location>
		<init ref="id45"/>
		<transition id="id47">
			<source ref="id46"/>
			<target ref="id46"/>
			<label kind="synchronisation" x="-229" y="-161">mixSupply?</label>
			<label kind="assignment" x="-229" y="-144">mix=getFlowWithPassage(o2Duty)</label>
			<nail x="-229" y="-101"/>
			<nail x="-170" y="-101"/>
		</transition>
		<transition id="id48">
			<source ref="id46"/>
			<target ref="id45"/>
			<label kind="guard" x="-348" y="-144">t&gt;=100</label>
			<label kind="assignment" x="-348" y="-161">t=0</label>
			<nail x="-322" y="-127"/>
		</transition>
		<transition id="id49">
			<source ref="id45"/>
			<target ref="id46"/>
			<label kind="synchronisation" x="-348" y="-33">O2on?</label>
			<label kind="assignment" x="-340" y="-8">t=0</label>
		</transition>
	</template>
	<template>
		<name>ExpValve</name>
		<declaration>//clock time;</declaration>
		<location id="id50" x="0" y="0">
			<name x="-42" y="-8">off</name>
		</location>
		<location id="id51" x="221" y="0">
			<name x="238" y="-8">on</name>
			<label kind="invariant" x="238" y="8">timeExpV&lt;=225</label>
		</location>
		<init ref="id50"/>
		<transition id="id52">
			<source ref="id51"/>
			<target ref="id50"/>
			<label kind="synchronisation" x="76" y="-110">expOff?</label>
			<label kind="assignment" x="76" y="-93">timeExpV=0</label>
			<nail x="110" y="-76"/>
		</transition>
		<transition id="id53">
			<source ref="id50"/>
			<target ref="id51"/>
			<label kind="synchronisation" x="68" y="0">expOn?</label>
			<label kind="assignment" x="68" y="17">timeExpV=0</label>
		</transition>
	</template>
	<system>//flowrate, FiO2, inputs
//airDuty, o2Duty, outputs
//config = Setup(flowrate, FiO2, airDuty, o2Duty);

//o2Valve = O2Valve(o2Duty, mix);
//airValve = Injector(airOn, airDuty, mix);

/**
expiration valve
**/
//expValve = ExpValve();

//Ventilator = Control(pInsp,pExp,tInsp,tExp, totalT);

system  Setup, O2Valve, Injector, ExpValve, Control;</system>
	<queries>
		<query>
			<formula>E&lt;&gt; Control.ExpPause</formula>
			<comment/>
			<result outcome="success" type="quality" timestamp="2023-12-13 06:05:10 +0100">
			</result>
		</query>
	</queries>
</nta>
